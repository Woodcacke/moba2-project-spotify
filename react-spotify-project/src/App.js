import React, { Component } from 'react';
import './App.css';
import SpotifyWebApi from 'spotify-web-api-js';
import { PlaylistList } from './PlaylistList'
import { Input, Button, Label, FormGroup, Form, Navbar, NavbarBrand } from 'reactstrap';
const spotifyApi = new SpotifyWebApi();

class App extends Component {
  constructor() {
    super();

    this.toggle = this.toggle.bind(this);
    const params = this.getHashParams();
    this.handleChange = this.handleChange.bind(this);
    this.getTracksOfPlaylist = this.getTracksOfPlaylist.bind(this);
    const token = params.access_token;
    if (token) {
      spotifyApi.setAccessToken(token);
      //spotifyApi.setPromiseImplementation();
    }
    this.state = {
      loggedIn: token ? true : false,
      user: 'woodcacke',
      nowPlaying: { name: 'Nothing is playing', albumArt: null },
      userPlaylist: [],
      tracks: [],
      collapse: false,
      modal: false
    }

  }
  toggle() {
    this.setState({ modal: !this.state.modal });
  }
  getHashParams() {
    var hashParams = {};
    var e, r = /([^&;=]+)=?([^&;]*)/g,
      q = window.location.hash.substring(1);
    e = r.exec(q)
    while (e) {
      hashParams[e[1]] = decodeURIComponent(e[2]);
      e = r.exec(q);
    }
    return hashParams;
  }

  getNowPlaying() {
    spotifyApi.getMyCurrentPlaybackState()
      .then((response) => {
        response ? this.setState({
          nowPlaying: {
            name: response.item.name,
            albumArt: response.item.album.images[0].url
          }
        }) : this.setState({
          nowPlaying: {
            name: 'nothing is playing',
            img: ''
          }
        });
      })
  }

  getPlaylistOfUser(user) {
    spotifyApi.getUserPlaylists(this.state.user)
      .then((response) => {
        this.handleUserPlaylistResponse(response);
      })
  }
  handleUserPlaylistResponse(response) {
    console.log(response);
    var playlists = [];
    for (var i = 0; i < response.items.length; i++) {
      var playlist = {
        name: response.items[i].name,
        id: response.items[i].id,
        ownerId: response.items[i].owner.id,
        public: response.items[i].public,
        tracks: {
          total: response.items[i].tracks.total,
          href: response.items[i].tracks.href
        },
        img: response.items[i].images[0],
        uri: response.items[i].uri
      }
      playlists.push(playlist);
      this.setState({ userPlaylist: playlists })
    }
  }
  getTracksOfPlaylist(userId, playlistId) {
    //console.log(href);
    this.toggle();
    spotifyApi.getPlaylistTracks(userId, playlistId)
      .then((response) => {
        console.log(response);
        console.log(this);
        this.handleTrackPlaylistResponse(response);
      })
  }

  handleTrackPlaylistResponse(response) {
    var tracks = [];
    for (var i = 0; i < response.items.length; i++) {
      var track = response.items[i].track;
      tracks.push(track);
      this.setState({ tracks: tracks })
    }

  }

  handleChange(event) {
    this.setState({ user: event.target.value });
  }
  render() {

    if (!this.state.loggedIn) {
      return (
        <div>
          <a href='http://localhost:8888' > Login to Spotify </a>
        </div>
      )
    }
    else {
      return (
        <div className="App" >
          <div>
            <Navbar color="light" light expand="md">
              <NavbarBrand href="/">MOBA2 React-Spotify</NavbarBrand>
            </Navbar>
          </div>

          <div style={ControlStyle}>
            <Form >
              <FormGroup row>
                <Label for="username" width='90%'>Username</Label>
                <Input placeholder="username" id="username" value={this.state.user} onChange={this.handleChange} />
                <Button outline color="secondary" onClick={() => this.getPlaylistOfUser()}>Check for Playlist</Button>
              </FormGroup>
            </Form>
          </div>
          <div>


            <div>
              <PlaylistList
                state={this.state}
                playlist={this.state.userPlaylist}
                tracks={this.state.tracks}
                getTracksOfPlaylist={this.getTracksOfPlaylist}
                toggle={this.toggle}
              />
            </div>

          </div>
          <div className="Footer" style={FooterStyle}>
            <div>
              {
                this.state.nowPlaying.albumArt != null
                  ? <div><img src={this.state.nowPlaying.albumArt} height="42" alt="Not available" />
                  {" " + this.state.nowPlaying.name}</div>
                  : <Button onClick={() => this.getNowPlaying()}>Check Now Playing</Button>
              }

            </div>
          </div>
        </div>

      );
    }
  }
}

const ControlStyle = {
  display: 'flex',
  flexDirection: 'row',
  minWidth: '100px',
  width: '100%',
  height: '40%',
  margin: '25px auto 25px auto'
};

const FooterStyle = {
  display: 'flex',
  // height: '7%',
  width: '100%',
  backgroundColor: '#F5F5F5',
  textAlign: 'center',
  // padding: '20px',
  zIndex: 9999,
  position: 'fixed',
  margin: 'auto 0px auto auto',
  bottom: '0',
};

export default App;
