import * as React from 'react';
import { ListGroup, ListGroupItem, Label } from 'reactstrap';

export const PlaylistTracks = ({ tracks }) => (
    <ListGroup>
        {
            tracks.map(track => (
                <ListGroupItem
                    key={track.id}
                    id={track.id.toString()}
                >
                    <div>
                        <img src={track.album.images[0].url} style={{ height: 150 }} alt={track.id} />
                    </div>
                    <div>
                        <Label for="username">{track.name}</Label>
                    </div>
                    <div>
                        <Label for="username">{track.artists[0].name}</Label>
                    </div>
                </ListGroupItem>
            ))
        }
    </ListGroup>
)