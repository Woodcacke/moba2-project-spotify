import * as React from 'react';
import { PlaylistTracks } from './PlaylistTracks'
import { ListGroup, ListGroupItem, Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';

export const PlaylistList = ({ state, playlist, getTracksOfPlaylist, tracks, toggle }) => (
    <ListGroup>
        {
            playlist.map(item => (
                <ListGroupItem
                    key={item.id}
                    id={item.id.toString()}
                >
                    <div>
                        <img src={item.img.url} style={{ height: 150 }} alt={item.id} />
                    </div>
                    <div>
                        {item.name}
                    </div>
                    <div>
                        <Button color="danger" onClick={() => getTracksOfPlaylist(item.ownerId, item.id)}>Get Tracks</Button>
                        <Modal isOpen={state.modal} toggle={toggle}>
                            <ModalHeader toggle={toggle}>Tracks</ModalHeader>
                            <ModalBody>
                                <PlaylistTracks tracks={tracks} />
                            </ModalBody>
                            <ModalFooter>
                                <Button color="secondary" onClick={toggle}>Cancel</Button>
                            </ModalFooter>
                        </Modal>
                    </div>
                </ListGroupItem>
            ))
        }
    </ListGroup>
)