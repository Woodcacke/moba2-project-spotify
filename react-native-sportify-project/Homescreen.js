import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Alert
} from 'react-native';
import { SpotifyApi } from './spotify.api.class';
import PlaylistList from './playlist.list';
import { createStackNavigator } from 'react-navigation';

export default class HomeScreen extends React.Component {
    _spotifyApi = null;
    static navigationOptions = {
		title: 'MOBA 2 - Spotify',
	};
    constructor() {
        super();
        this.state = { spotifyInitialized: false, loggedIn: false };
        this.spotifyLoginButtonWasPressed = this.spotifyLoginButtonWasPressed.bind(this);
    }

    componentDidMount() {
        this._spotifyApi = new SpotifyApi();
    }


    spotifyLoginButtonWasPressed() {
        // log into Spotify
        this._spotifyApi.login().then((loggedIn) => {
            this.setState({ loggedIn: loggedIn });
            if (loggedIn) {
                this.props.navigation.navigate('Playlist', { Spotify: this._spotifyApi });
            }
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>To be able to use this app,</Text>
                <Text>you need to be logged in to Spotify!</Text>
                <Text>After logging in,</Text>
                <Text>you will automatically be redirected to your Playlists!</Text>
                <TouchableHighlight onPress={this.spotifyLoginButtonWasPressed} style={styles.spotifyLoginButton}>
                    <Text style={styles.spotifyLoginButtonText}>Log in to Spotify</Text>
                </TouchableHighlight>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    spotifyLoginButton: {
        justifyContent: 'center',
        borderRadius: 18,
        backgroundColor: 'green',
        overflow: 'hidden',
        width: 200,
        height: 40,
        margin: 20,
    },
    spotifyLoginButtonText: {
        fontSize: 20,
        textAlign: 'center',
        color: 'white',
    },

});

