import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, FlatList, Alert, TouchableWithoutFeedback } from 'react-native';

const extractKey = () => '1';

export default class TrackList extends Component {
  spotify;
  interval;
  static navigationOptions = {
		title: 'Tracks deiner Playliste',
	};
  constructor(props) {
    super(props);
    this.state = { data: null, error: '', nowPlaying: this.props.navigation.getParam('NowPlaying', '') };
    spotify = this.props.navigation.getParam('Spotify', 'ups');
    let id = this.props.navigation.getParam('Id', '');
    let user = this.props.navigation.getParam('User', '');
    spotify.getTracksFromPlaylist(user, id).then((result) => {
      this.setState({ data: result });
      //Alert.alert('Success', JSON.stringify(result, null, 4));
    }).catch((error) => {
      this.setState({ error: error.message });
      Alert.alert('Ups something went wrong!', error.message);
    });

    this.interval = setInterval(() => {
      spotify.getActualPlaying().then((actualPlaying) => {
        this.setState({ nowPlaying: actualPlaying });
      }).catch(() => {
        Alert.alert('Error while getting actual playing!', error.message);
      });
    }, 1000);
  }

  componentWillUnmount(){
    if(this.interval){
      clearInterval(this.interval);
      this.interval = undefined;
    }
  }

  trackClicked(item){
    Alert.alert('Track Clicked', item.track.name);
  }

  renderItem = ({ item }) => {
    return (
      <TouchableWithoutFeedback onPress={() => this.trackClicked(item)}>
      <View style={styles.row}>
        <Image style={styles.image} source={{ uri: item.track.album.images[0].url }} />
        <View style={{ flex: 1 }}>
          <Text style={styles.popularity}>
            {item.track.name}
          </Text>
          <Text style={styles.popularity}>
            
          </Text>
        </View>
      </View>
      </TouchableWithoutFeedback>
    )
  }
  render() {
    let myData = this.state.data ? this.state.data.items : [];
    return (

      <View style={styles.container}>
        <FlatList

          data={myData}
          renderItem={this.renderItem}
          keyExtractor={extractKey}
        />
        <View style={this.state.nowPlaying ? styles.footer : styles.noFooter}>
          <Text style={styles.footerText}>Läuft gerade: {this.state.nowPlaying}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  list: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  popularity: {
    flex: 1,
    paddingLeft: 15,
    color: '#777',
    fontWeight: 'bold',
  },
  row: {
    flexDirection: 'row',
    padding: 15,
    marginBottom: 5,
    backgroundColor: '#111111',
    alignItems: 'stretch',
  },
  image: {
    height: 64,
    width: 64,
  },
  footer: {
    flex: 0,
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  noFooter: {
    height: 0,
    overflow: 'hidden'
  },
  footerText: {
    margin: 10,
    fontSize: 20,
    color: 'white',
  },
});
