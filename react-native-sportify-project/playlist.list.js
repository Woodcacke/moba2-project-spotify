import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, FlatList, Alert, TouchableWithoutFeedback } from 'react-native';

const extractKey = () => '1';

export default class PlaylistList extends Component {
  spotify;
  interval;
  static navigationOptions = {
    title: 'Playlisten',
  };
  constructor(props) {
    super(props);
    this.state = { data: null, error: '', nowPlaying: '' };
    spotify = this.props.navigation.getParam('Spotify', 'ups');
    spotify.getMyPlaylists().then((result) => {
      this.setState({ data: result });
    }).catch((error) => {
      this.setState({ error: error.message });
      Alert.alert('Ups, something went wrong!', error.message);
    });

    this.interval = setInterval(() => {
      spotify.getActualPlaying().then((actualPlaying) => {
        this.setState({ nowPlaying: actualPlaying });
      }).catch(() => {
        Alert.alert('Error while getting actual playing!', error.message);
      });
    }, 1000);
  }

  componentWillUnmount(){
    if(this.interval){
      clearInterval(this.interval);
      this.interval = undefined;
    }
  }

  playlistClicked(item) {
    this.props.navigation.navigate("Tracks", { 
      Id: item.id, 
      Spotify: spotify, 
      User: item.owner.id,
      NowPlaying: this.state.nowPlaying 
    });
  }

  renderItem = ({ item }) => {
    return (
      <TouchableWithoutFeedback onPress={() => this.playlistClicked(item)}>
        <View style={styles.row}>
          <Image style={styles.image} source={{ uri: item.images[0].url }} />
          <View style={{ flex: 1 }}>
            <Text style={styles.popularity}>
              {item.name}
            </Text>
            <Text style={styles.popularity}>
              Anzahl Titel: {item.tracks.total}
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }
  render() {
    let myData = this.state.data ? this.state.data.items : [];
    return (

      <View style={styles.container}>
        <FlatList

          data={myData}
          renderItem={this.renderItem}
          keyExtractor={extractKey}
        />
        <View style={this.state.nowPlaying ? styles.footer : styles.noFooter}>
          <Text style={styles.footerText}>Läuft gerade: {this.state.nowPlaying}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  list: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  popularity: {
    flex: 1,
    paddingLeft: 15,
    color: '#777',
    fontWeight: 'bold',
  },
  row: {
    flexDirection: 'row',
    padding: 15,
    marginBottom: 5,
    backgroundColor: '#111111',
    alignItems: 'stretch',
  },
  image: {
    height: 64,
    width: 64,
  },

  footer: {
    flex: 0,
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  noFooter: {
    height: 0,
    overflow: 'hidden'
  },
  footerText: {
    margin: 10,
    fontSize: 20,
    color: 'white',
  },
});
