import Spotify from 'rn-spotify-sdk';

export class SpotifyApi {
    Initialized = false;
    LoggedIn = false;

    constructor() {
        Spotify.initialize({
            "clientID": "fd40491f9bd14561bf47786c2426e819",
            "sessionUserDefaultsKey": "SpotifySession",
            "redirectURL": "reactnativesportifyproject://auth",
            "scopes": ["user-read-private", "playlist-read", "playlist-read-private", "streaming", "user-read-currently-playing"],
        }).then((initialized) => {
            this.Initialized = true;
        }).catch((error) => {
        });
    }

    login() {
        return new Promise((resolve, reject) => {
            Spotify.login().then((loggedIn) => {
                if (loggedIn) {
                    this.LoggedIn = true;
                    resolve(true);
                }
                else {
                    // cancelled
                    resolve(false);
                }
            }).catch((error) => {
                // error
                Alert.alert("Error", error.message);
                resolve(false);
            });
        });
    }

    getMyPlaylists() {
        return new Promise((resolve, reject) => {
            Spotify.sendRequest('v1/me/playlists', 'GET', null, false).then((result) => {
                resolve(result);
            }).catch((error) => {
                reject(error);
            });
        });

    }

    getTracksFromPlaylist(user, id){
        return new Promise((resolve, reject) => {
            Spotify.sendRequest('v1/users/' + user + '/playlists/' + id + '/tracks', 'GET', null, false).then((result) => {
                resolve(result);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    getActualPlaying(){
        return new Promise((resolve, reject) => {
            Spotify.sendRequest('v1/me/player/currently-playing', 'GET', null, false).then((result) => {
                if(result && result.item && result.item.name){
                    resolve(result.item.name);
                } else {
                    resolve('');
                }
            }).catch((error) => {
                reject(error);
            });
        });
    }

}