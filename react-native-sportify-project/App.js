import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	TouchableHighlight,
	Alert
} from 'react-native';
import { SpotifyApi } from './spotify.api.class';
import PlaylistList from './playlist.list';
import { createStackNavigator } from 'react-navigation';
import HomeScreen from './Homescreen';
import TrackList from './track.list';

const RootStack = createStackNavigator(
	{
		Home: {
			screen: HomeScreen
		},
		Playlist: {
			screen: PlaylistList
		},
		Tracks: {
			screen: TrackList
		}
	},
	{
		initialRouteName: 'Home',
		navigationOptions: {
			headerStyle: {
				backgroundColor: 'black',
			},
			headerTintColor: '#777',
			headerTitleStyle: {
				fontWeight: 'bold',
			},
		},
	}
);

export default class App extends React.Component {
	static nowPlaying = 'hello';
	render() {
		return (
		<View style={styles.container}>
			<RootStack style={styles.content}/>
		</View>);
	}
}

const styles = StyleSheet.create({
    container: {
		flex: 1,
	},
	content: {
		flex: 1
	}
});

